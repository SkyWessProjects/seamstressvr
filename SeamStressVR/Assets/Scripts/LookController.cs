using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookController : MonoBehaviour
{
	public float minX = -60f;
	public float maxX = 60f;

	public float sensitivity;
	public Camera cam;

	float rotY = 0f;
	float rotX = 0f;

	public GameObject holdPoint;
	private GameObject held;
	private GameObject sewObject;

	public GameObject pointPrefab;
	private GameObject[] sewParent = { };
	private int indexOfSewParent = 0;

	void Start()
	{
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
		cam = GetComponent<Camera>();
	}

	void Update()
	{
		MoveCam();
		if (Input.GetMouseButtonDown(0))
			StartHold();
		if (Input.GetMouseButtonUp(0))
			EndHold();
		if (Input.GetMouseButtonDown(1))
			Sew();
		if(indexOfSewParent > 1)
        {
			for(int i = 0; i<indexOfSewParent; i++)
			{
				sewParent[i].transform.Translate(Vector3.one, sewParent[i+1].transform);
				i++;
			}
        }

	}
    private void StartHold()
    {

		var ray = transform.GetComponent<Camera>().ViewportPointToRay(Vector3.one * 0.5f);
		RaycastHit hit;
		if (!held && Physics.Raycast(ray, out hit, 5.5f) && hit.transform.gameObject.tag == "Holdable")
		{
			held = hit.transform.gameObject;
			if(hit.transform.parent != null)
			held = hit.transform.parent.gameObject;
			held.transform.SetParent(holdPoint.transform);
			for(int i = 0; i < held.transform.childCount; i++)
            {
				held.transform.GetChild(i).GetComponent<Rigidbody>().isKinematic = true;
				held.transform.GetChild(i).GetComponent<Rigidbody>().useGravity = false;
			}
		}
	}
    private void EndHold()
	{
		if (held)
		{
			for (int i = 0; i < held.transform.childCount; i++)
			{
				held.transform.GetChild(i).GetComponent<Rigidbody>().isKinematic = false;
				held.transform.GetChild(i).GetComponent<Rigidbody>().useGravity = true;
			}
			held.transform.parent = null;
			held = null;
		}
	}

	private void Sew()
	{

		var ray = transform.GetComponent<Camera>().ViewportPointToRay(Vector3.one * 0.5f);
		RaycastHit hit;
		if (!held && !sewObject && Physics.Raycast(ray, out hit, 5.5f) && hit.transform.gameObject.tag == "Holdable")
		{
			sewParent[indexOfSewParent] = Instantiate(pointPrefab, hit.point, Quaternion.identity);
			sewObject = hit.transform.gameObject;
			sewObject.transform.SetParent(pointPrefab.transform);
			sewObject = null;
			indexOfSewParent++;
		}
	}
	private void MoveCam()
    {
		rotY += Input.GetAxis("Mouse X") * sensitivity;
		rotX += Input.GetAxis("Mouse Y") * sensitivity;

		rotX = Mathf.Clamp(rotX, minX, maxX);

		transform.localEulerAngles = new Vector3(0, rotY, 0);
		cam.transform.localEulerAngles = new Vector3(-rotX, rotY, 0);

		if (Input.GetKeyDown(KeyCode.Escape))
		{
			//Mistake happened here vvvv
			Cursor.lockState = CursorLockMode.None;
			Cursor.visible = true;
		}

		if (Cursor.visible && Input.GetMouseButtonDown(1))
		{
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;
		}
    }
}